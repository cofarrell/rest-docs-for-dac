#!/usr/bin/env ruby

require 'rubygems'
require 'mechanize'
require 'optparse'

options = {}
opt_parser = OptionParser.new do |opt|
  opt.banner = "Usage: ./run.rb [OPTIONS] URL"
  opt.separator  ""
  opt.separator  "Options"

  opt.on("-s","--single-page","Process a single page (i.e., not an index of REST docs)") do |s|
    options[:single_page] = s
  end
  opt.on("-p", "--path PATH", "Relative path to assets for the generated page. [Default: '../..']") do |s|
    options[:path] = s
  end
end
opt_parser.parse!

agent = Mechanize.new
agent.max_file_buffer = nil

#url = 'http://docs.atlassian.com/jira/REST/'
url = ARGV[0]
path = options[:path] || '../..'
prod = url.split('/')[3]
puts "Getting #{url}"

page = agent.get(url)

def process_page(link, prod, options, path)
  header_template = File.read('template/header.html')
  footer_template = File.read('template/footer.html')

  begin
    if options[:single_page]
      puts "Loading #{link.link.href}"
      doc = link
      link = link.link
    else
      puts "Loading #{link.href}"
      doc = link.click
    end
    filename = doc.uri.path.split('/')[-1]

    puts "  Got: #{doc.title}"

    doc_parsed = doc.parser
    html = doc.parser

    html.search('.//style').remove
    #html.search('.//script').remove

    head = html.at('head')
    css = Nokogiri::XML::Node.new "link", doc_parsed
    css['rel'] = 'stylesheet'
    css['href'] = 'https://developer.atlassian.com/static/javadoc/jira/5.0.5/assets/doclava-developer-docs.css'
    head << css
    
    css = Nokogiri::XML::Node.new "link", doc_parsed
    css['rel'] = 'stylesheet'
    css['href'] = 'https://developer.atlassian.com/static/javadoc/jira/5.0.5/assets/customizations.css'
    head << css

    css = Nokogiri::XML::Node.new "link", doc_parsed
    css['rel'] = 'stylesheet'
    css['href'] = "#{path}/assets/css/rest.css"
    head << css

=begin
    script = Nokogiri::XML::Node.new "script", doc_parsed
    script['type'] = 'text/javascript'
    script['src'] = "#{path}/assets/js/libs.js"
    head << script

    script = Nokogiri::XML::Node.new "script", doc_parsed
    script['type'] = 'text/javascript'
    script['src'] = "#{path}/assets/js/app.js"
    head << script
=end

    body_contents = doc_parsed.css('body').children

    header = Nokogiri::HTML.fragment(header_template)
    header.css('#logo img').first['src'] = "#{path}/assets/images/atlassian-logo.png"

    footer = Nokogiri::HTML.fragment(footer_template)

    container = Nokogiri::XML::Node.new "div", doc_parsed
    container['class'] = 'container'
    container << body_contents
    body = doc_parsed.at_css('body')
    body << header
    body << container
    body << footer

    dirname = 'rest/' + prod
    unless FileTest::directory?(dirname)
      Dir::mkdir dirname
    end
    File.open(dirname + '/'+ filename + '.html','w') do |f|
      f.write(html.to_html)
    end
  rescue => e
    $stderr.puts $!.inspect
  end

end

if options[:single_page]
  process_page page, prod, options, path
else
  page.links.each do |link|
    next if link.href =~ /^\?/ or link.href == "/#{prod}/"
    process_page link, prod, options, path
  end
end
