REST API Beautifier for DAC
===========================

This is a simple hacky way to beautify the Atlassian REST API docs 
for DAC.

Usage
-----

    $ gem install bundler
    $ bundle install
    $ ruby run.rb <url to rest docs (i.e., http://docs.atlassian.com/jira/REST/)>

This script will run through all of the available REST API docs and add the appropriate
HTML and CSS to make them look like DAC.