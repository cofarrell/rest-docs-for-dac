all: clean
	ruby run.rb http://docs.atlassian.com/jira/REST/
	ruby run.rb http://docs.atlassian.com/confluence/REST/
	ruby run.rb http://docs.atlassian.com/bamboo/REST/

clean:
	rm -rf rest/*
